# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Module finding HJets in the LCG release. Defines:
#  - MCFM_FOUND
#  - MCFM_INCLUDE_DIR
#  - MCFM_INCLUDE_DIRS
#  - MCFM_LIBRARIES
#  - MCFM_LIBRARY_DIRS
#
# Can be steered by MCFM_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# Declare the external module:
lcg_external_module( NAME MCFM
   INCLUDE_SUFFIXES include INCLUDE_NAMES MCFM/CXX_Wrapper.h
   LIBRARY_SUFFIXES lib lib64
   DEFAULT_COMPONENTS MCFM )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( MCFM DEFAULT_MSG MCFM_INCLUDE_DIR
   MCFM_LIBRARIES )
mark_as_advanced( MCFM_FOUND MCFM_INCLUDE_DIR MCFM_INCLUDE_DIRS
   MCFM_LIBRARIES MCFM_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( mcfm )
