# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# - Locate cln library
# Defines:
#
#  CLN_FOUND
#  CLN_INCLUDE_DIR
#  CLN_INCLUDE_DIRS
#  CLN_<component>_LIBRARY
#  CLN_<component>_FOUND
#  CLN_LIBRARIES
#  CLN_LIBRARY_DIRS
#
# Can be steered by CLN_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# First just find the headers:
lcg_external_module( NAME cln
  INCLUDE_SUFFIXES include INCLUDE_NAMES cln/cln.h
  LIBRARY_SUFFIXES lib lib64
  DEFAULT_COMPONENTS cln)

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( cln DEFAULT_MSG
  CLN_INCLUDE_DIR CLN_LIBRARIES )
mark_as_advanced( CLN_FOUND CLN_INCLUDE_DIR CLN_INCLUDE_DIRS
  CLN_LIBRARIES CLN_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( cln )
