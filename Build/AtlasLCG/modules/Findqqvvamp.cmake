# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# - Locate qqvvamp library
# Defines:
#
#  QQVVAMP_FOUND
#  QQVVAMP_INCLUDE_DIR
#  QQVVAMP_INCLUDE_DIRS
#  QQVVAMP_<component>_LIBRARY
#  QQVVAMP_<component>_FOUND
#  QQVVAMP_LIBRARIES
#  QQVVAMP_LIBRARY_DIRS
#
# Can be steered by QQVVAMP_LCGROOT.
#

# The LCG include(s):
include( LCGFunctions )

# First just find the headers:
lcg_external_module( NAME qqvvamp
  INCLUDE_SUFFIXES include INCLUDE_NAMES qqvvamp.h
  LIBRARY_SUFFIXES lib lib64
  DEFAULT_COMPONENTS qqvvamp)

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( qqvvamp DEFAULT_MSG
  QQVVAMP_INCLUDE_DIR QQVVAMP_LIBRARIES )
mark_as_advanced( QQVVAMP_FOUND QQVVAMP_INCLUDE_DIR QQVVAMP_INCLUDE_DIRS
  QQVVAMP_LIBRARIES QQVVAMP_LIBRARY_DIRS )

# Set up the RPM dependency:
lcg_need_rpm( qqvvamp )
