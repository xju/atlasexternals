# Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
#
# Package building Acts as part of the offline / analysis software build.
#

# The name of the package:
atlas_subdir( Acts )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# External(s) needed by Acts:
find_package( Eigen3 )
find_package( Boost )
if( NOT ATLAS_BUILD_NLOHMANN_JSON )
   find_package( nlohmann_json )
endif()

# Tell the user what's happening:
message( STATUS "Building Acts as part of this project" )

# Temporary directory for the build results:
set( _buildDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ActsBuild" )
# Directory holding the "stamp" files:
set( _stampDir "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ActsStamp" )

# List of paths given to CMAKE_PREFIX_PATH.
set( _prefixPaths "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   $ENV{CMAKE_PREFIX_PATH} ${BOOST_LCGROOT} ${Eigen3_DIR} ${nlohmann_json_DIR} )

# Extra configuration options for Acts:
set( _extraOptions )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Release" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=Release )
elseif( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Declare where to get Acts from.
set( ATLAS_ACTS_SOURCE
   "URL;https://github.com/acts-project/acts/archive/refs/tags/v37.3.0.tar.gz;URL_HASH;SHA256=7062f00392c02ec073d0a94cf2b3598011288a5d93bf143fc59d927d8329f98e"
   CACHE STRING "The source for Acts" )
mark_as_advanced( ATLAS_ACTS_SOURCE )

# Decide whether / how to patch the Acts sources.
set( ATLAS_ACTS_PATCH "" CACHE STRING "Patch command for Acts" )
set( ATLAS_ACTS_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of Acts (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_ACTS_PATCH ATLAS_ACTS_FORCEDOWNLOAD_MESSAGE )

# Build Acts for the build area:
ExternalProject_Add( Acts
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   ${ATLAS_ACTS_SOURCE}
   ${ATLAS_ACTS_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_INCLUDEDIR:PATH=${CMAKE_INSTALL_INCLUDEDIR}/Acts
   -DCMAKE_INSTALL_LIBDIR:PATH=${CMAKE_INSTALL_LIBDIR}
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD}
   -DCMAKE_CXX_EXTENSIONS:BOOL=${CMAKE_CXX_EXTENSIONS}
   -DACTS_BUILD_PLUGIN_JSON:BOOL=ON
   -DACTS_BUILD_PLUGIN_GEOMODEL:BOOL=ON
   -DACTS_BUILD_FATRAS:BOOL=ON
   -DACTS_USE_SYSTEM_NLOHMANN_JSON:BOOL=ON # do not build separately
   -DCMAKE_PREFIX_PATH:PATH=${_prefixPaths}
   ${_extraOptions}
   LOG_CONFIGURE 1 )
# Need to modify the printout here when the Acts version is updated, and we want
# to ensure a clean build.
ExternalProject_Add_Step( Acts forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_ACTS_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   ALWAYS TRUE
   DEPENDERS download )
ExternalProject_Add_Step( Acts purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results of Acts"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( Acts forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of Acts"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS TRUE )
ExternalProject_Add_Step( Acts buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing Acts into the build area"
   DEPENDEES install )
add_dependencies( Package_Acts Acts )
add_dependencies( Acts nlohmann_json )
add_dependencies( Acts GeoModel )

# And now install it:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install the FindActs.cmake module.
install( FILES "cmake/FindActs.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
