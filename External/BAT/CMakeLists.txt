# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building BAT as part of an analysis release
#

# The name of the package:
atlas_subdir( BAT )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 OLD )
endif()

# Set ROOTSYS based on the build environment:
if( ATLAS_BUILD_ROOT )
   set( _rootsys "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}" )
else()
   find_package( ROOT REQUIRED )
   set( _rootsys "${ROOTSYS}" )
endif()

# Declare where to get BAT from.
set( ATLAS_BAT_SOURCE
   "URL;http://cern.ch/atlas-computing/projects/RootCoreExternal/v013/BAT-0.9.4.1.tar.gz;URL_MD5;e1a09dc5e379dd5b5d665729ea45b535"
   CACHE STRING "The source for BAT" )
mark_as_advanced( ATLAS_BAT_SOURCE )

# Decide whether / how to patch the BAT sources.
set( ATLAS_BAT_PATCH
   "PATCH_COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/BAT-0.9.4.1-dict.patch;COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/BAT-0.9.4.1-cxx17.patch;COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/BAT-0.9.4.1-root624.patch"
   CACHE STRING "Patch command for BAT" )
set( ATLAS_BAT_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of BAT (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_BAT_PATCH ATLAS_BAT_FORCEDOWNLOAD_MESSAGE )

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/BATBuild )

# Extra flag(s) for the configuration:
set( _extraConf )
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" OR
      "${CMAKE_BUILD_TYPE}" STREQUAL "RelWithDebInfo" )
   set( _extraConf --enable-debug )
endif()

# Create the script that will sanitize the BAT installation:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeBAT.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeBAT.sh
   @ONLY )

# Set up the build of RooUnfold for the build area:
ExternalProject_Add( BAT
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   ${ATLAS_BAT_SOURCE}
   ${ATLAS_BAT_PATCH}
   CONFIGURE_COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh"
   "<SOURCE_DIR>/configure" --with-rootsys=${_rootsys}
   --prefix=${_buildDir} ${_extraConf}
   BUILD_COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh" make
   INSTALL_COMMAND
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/atlas_build_run.sh" make install
   COMMAND "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeBAT.sh"
   COMMAND ${CMAKE_COMMAND} -E copy_directory "${_buildDir}/" "<INSTALL_DIR>" )
ExternalProject_Add_Step( BAT forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_BAT_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( BAT purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for BAT"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
add_dependencies( Package_BAT BAT )
if( ATLAS_BUILD_ROOT )
   add_dependencies( BAT ROOT )
endif()

# Install BAT
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

# Install FindBAT.cmake
install( FILES "cmake/FindBAT.cmake"
   DESTINATION "${CMAKE_INSTALL_CMAKEDIR}/modules" )
