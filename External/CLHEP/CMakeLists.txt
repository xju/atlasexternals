# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Package building CLHEP as part of the offline build procedure.
#

# The name of the package:
atlas_subdir( CLHEP )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()

# Stop here if CLHEP is not to be built.
if( NOT ATLAS_BUILD_CLHEP )
   return()
endif()

# Let the user know what's happening.
message( STATUS "Building CLHEP as part of this project" )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get CLHEP from.
set( ATLAS_CLHEP_SOURCE
   "URL;https://gitlab.cern.ch/atlas-sw-git/CLHEP/-/archive/CLHEP_2_4_1_3_atl04/CLHEP-CLHEP_2_4_1_3_atl04.tar.gz;URL_MD5;d2f9ea9f3368d2dd0321457a222c3f7e"
   CACHE STRING "The source for CLHEP" )
mark_as_advanced( ATLAS_CLHEP_SOURCE )

# Decide whether / how to patch the CLHEP sources.
set( ATLAS_CLHEP_PATCH "" CACHE STRING "Patch command for CLHEP" )
set( ATLAS_CLHEP_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of CLHEP (2022.10.11.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_CLHEP_PATCH ATLAS_CLHEP_FORCEDOWNLOAD_MESSAGE )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" EQUAL 11 )
   list( APPEND _extraOptions -DCLHEP_BUILD_CXXSTD:STRING=-std=c++11 )
elseif( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL 14 ) # CLHEP is not compatible with C++17
   list( APPEND _extraOptions -DCLHEP_BUILD_CXXSTD:STRING=-std=c++14 )
endif()

if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Directory for the temporary build results:
set( _buildDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CLHEPBuild" )
# Directory holding the "stamp" files.
set( _stampDir
   "${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CLHEPStamp" )

# Build CLHEP in the build area:
ExternalProject_Add( CLHEP
   PREFIX "${CMAKE_BINARY_DIR}"
   INSTALL_DIR "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
   STAMP_DIR "${_stampDir}"
   ${ATLAS_CLHEP_SOURCE}
   ${ATLAS_CLHEP_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCLHEP_BUILD_DOCS:BOOL=OFF
   ${_extraOptions}
   LOG_CONFIGURE 1 )
# NB need to modify the printout here in the case of a CLHEP version change
ExternalProject_Add_Step( CLHEP forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_CLHEP_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( CLHEP purgeBuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for CLHEP"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( CLHEP forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f "<BINARY_DIR>/CMakeCache.txt"
   COMMENT "Forcing the (re-)configuration of CLHEP"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( CLHEP buildinstall
   COMMAND ${CMAKE_COMMAND} -E copy_directory
   "${_buildDir}/" "<INSTALL_DIR>"
   COMMENT "Installing CLHEP into the build area"
   DEPENDEES install )
add_dependencies( Package_CLHEP CLHEP )

# Install CLHEP:
install( DIRECTORY "${_buildDir}/"
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
