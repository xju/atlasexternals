Offline Software Python Modules
===============================

This package collects the Python modules that are needed by the ATLAS offline
software project(s), and which are not part of [LCG](http://lcginfo.cern.ch).
The modules are built and installed using `pip install`. The sources are exclusively
retrieved from our own [Python package repository](http://atlas-software-dist-eos.web.cern.ch/atlas-software-dist-eos/externals/PyModules/repo) hosted on EOS.

To add a new package follow these steps:

1. Download the source and wheel files from [PyPI](pypi.org) and copy them to our
   [repository](http://atlas-software-dist-eos.web.cern.ch/atlas-software-dist-eos/externals/PyModules/repo)
   on EOS. Create an ATLINFR Jira ticket if you don't have permissions yourself. The directory structure
   is described in [hosting your own index](https://packaging.python.org/guides/hosting-your-own-index/).
2. Add the package and their version to the [`requirements.txt.in`](External/PyModules/requirements.txt.in) file.
3. Update the CHANGES file in our [repository](http://atlas-software-dist-eos.web.cern.ch/atlas-software-dist-eos/externals/PyModules/repo)
