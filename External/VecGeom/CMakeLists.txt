# Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
#
# Package building VecGeom for the offline builds.
#

# The name of the package:
atlas_subdir( VecGeom )

# Silence ExternalProject warnings with CMake >=3.24.
if( POLICY CMP0135 )
   cmake_policy( SET CMP0135 NEW )
endif()

# Declare where to get VecGeom from.
set( ATLAS_VECGEOM_SOURCE
   "URL;https://gitlab.cern.ch/VecGeom/VecGeom/-/archive/v1.1.20/VecGeom-v1.1.20.tar.gz;URL_MD5;280e88becaa0e7d3b6377db6da7e1b9c"
   CACHE STRING "The source for VecGeom" )
mark_as_advanced( ATLAS_VECGEOM_SOURCE )

# Decide whether / how to patch the VecGeom sources.
set( ATLAS_VECGEOM_PATCH
   "PATCH_COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/VecGeom-1.1.20-kConeTolerance.patch;COMMAND;patch;-p1;-i;${CMAKE_CURRENT_SOURCE_DIR}/patches/VecGeom-1.1.20-VecCoreBuild.patch"
   CACHE STRING "Patch command for VecGeom" )
set( ATLAS_VECGEOM_FORCEDOWNLOAD_MESSAGE
   "Forcing the re-download of VecGeom (2022.10.23.)"
   CACHE STRING "Download message to update whenever patching changes" )
mark_as_advanced( ATLAS_VECGEOM_PATCH ATLAS_VECGEOM_FORCEDOWNLOAD_MESSAGE )

# Extra options for the configuration:
set( _extraOptions )
if( "${CMAKE_CXX_STANDARD}" GREATER_EQUAL "11" )
   list( APPEND _extraOptions -DCMAKE_CXX_STANDARD:STRING=${CMAKE_CXX_STANDARD} )
endif()

if( NOT "${CMAKE_BUILD_TYPE}" STREQUAL "" )
   list( APPEND _extraOptions -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE} )
endif()

# Enable GDML persistency for VecGeom.
find_package( XercesC )
list( APPEND _extraOptions
   -DGDML:BOOL=TRUE
   -DXercesC_INCLUDE_DIR:PATH=${XercesC_INCLUDE_DIR}
   -DXercesC_LIBRARY:FILEPATH=${XercesC_LIBRARY} )

# Decide whether to enable LTO
option( ATLAS_VECGEOM_USE_LTO "Build VecGeom libraries with Link Time Optimization" OFF )
if( ATLAS_VECGEOM_USE_LTO )
   list( APPEND _extraOptions -DCMAKE_INTERPROCEDURAL_OPTIMIZATION:BOOL=ON )
endif()

# Temporary directory for the build results:
set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VecGeomBuild )
# Directory holding the "stamp" files.
set( _stampDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VecGeomStamp )

# Create the script that will sanitize the geant4-config script after the build:
configure_file( ${CMAKE_CURRENT_SOURCE_DIR}/cmake/sanitizeConfig.sh.in
   ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh @ONLY )

# Build VecGeom for the build area:
ExternalProject_Add( VecGeom
   PREFIX ${CMAKE_BINARY_DIR}
   INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
   STAMP_DIR ${_stampDir}
   ${ATLAS_VECGEOM_SOURCE}
   ${ATLAS_VECGEOM_PATCH}
   CMAKE_CACHE_ARGS
   -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
   -DCMAKE_INSTALL_LIBDIR:STRING=lib
   -DBUILTIN_VECCORE:BOOL=ON
   -DVECGEOM_BACKEND:STRING=Scalar
   -DBUILD_TESTING:BOOL=FALSE
   ${_extraOptions}
   LOG_CONFIGURE 1 )
ExternalProject_Add_Step( VecGeom forcedownload
   COMMAND ${CMAKE_COMMAND} -E echo "${ATLAS_VECGEOM_FORCEDOWNLOAD_MESSAGE}"
   INDEPENDENT TRUE
   DEPENDERS download )
ExternalProject_Add_Step( VecGeom purgebuild
   COMMAND ${CMAKE_COMMAND} -E remove_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E make_directory "<BINARY_DIR>"
   COMMAND ${CMAKE_COMMAND} -E remove_directory "${_buildDir}"
   COMMENT "Removing previous build results for VecGeom"
   INDEPENDENT TRUE
   DEPENDEES download
   DEPENDERS patch )
ExternalProject_Add_Step( VecGeom forceconfigure
   COMMAND ${CMAKE_COMMAND} -E remove -f <BINARY_DIR>/CMakeCache.txt
   COMMENT "Forcing the configuration of VecGeom"
   DEPENDEES update
   DEPENDERS configure
   ALWAYS 1 )
ExternalProject_Add_Step( VecGeom buildinstall
   COMMAND ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/sanitizeConfig.sh
   COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir}/ <INSTALL_DIR>
   COMMENT "Installing VecGeom into the build area"
   DEPENDEES install )
add_dependencies( Package_VecGeom VecGeom )

# Install VecGeom:
install( DIRECTORY ${_buildDir}/
   DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )
