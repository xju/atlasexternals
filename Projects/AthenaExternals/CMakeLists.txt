# Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
#
# Project file for the AthenaExternals project.
#

# Set up the project.
cmake_minimum_required( VERSION 3.25 )
project( AthenaExternals VERSION 22.0.0 LANGUAGES C CXX )

# Make sure that all _ROOT variables *are* used when they are set.
if( POLICY CMP0074 )
   cmake_policy( SET CMP0074 NEW )
endif()
# Handle CMP0072 by actively selecting the legacy OpenGL library by default.
set( OpenGL_GL_PREFERENCE "LEGACY" CACHE BOOL
   "Prefer using /usr/lib64/libGL.so unless the user says otherwise" )

# Set where to pick up AtlasCMake, AtlasLCG and AtlasHIP from.
set( AtlasCMake_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
set( AtlasHIP_DIR "${CMAKE_SOURCE_DIR}/../../Build/AtlasHIP"
   CACHE PATH "Location of AtlasLCG" )
mark_as_advanced( AtlasCMake_DIR LCG_DIR AtlasHIP_DIR )

# Find the ATLAS CMake code:
find_package( AtlasCMake REQUIRED )

# Set up which LCG version to use:
set( LCG_VERSION_POSTFIX "a_ATLAS_3"
   CACHE STRING "Post-fix for the LCG version number" )
set( LCG_VERSION_NUMBER 106
   CACHE STRING "LCG version number to use for the project" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED EXACT )

# Set up CUDA from the LCG release, if it is available.
find_package( CUDAToolkit )

# Set up AtlasHIP.
find_package( AtlasHIP REQUIRED )

# Additional package(s) needed for the build/runtime environment.
find_package( Vc )
find_package( VDT )

# Make CMake aware of the files in the cmake/ subdirectory.
list( INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake )

# Load the project's build options.
add_subdirectory( ProjectOptions
   "${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/ProjectOptions" )

# Set up CTest:
atlas_ctest_setup()

# Set up the ATLAS project.
atlas_project( PROJECT_ROOT ${CMAKE_SOURCE_DIR}/../../ )

# Set up some custom environment variables for the project:
set( AthenaExternalsEnvironment_DIR
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY} )
configure_file(
   ${CMAKE_SOURCE_DIR}/cmake/AthenaExternalsEnvironmentConfig.cmake.in
   ${AthenaExternalsEnvironment_DIR}/AthenaExternalsEnvironmentConfig.cmake
   @ONLY )
find_package( AthenaExternalsEnvironment )

# Configure and install the pre/post-configuration file(s).
configure_file( ${CMAKE_SOURCE_DIR}/cmake/PreConfig.cmake.in
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PreConfig.cmake @ONLY )
configure_file( ${CMAKE_SOURCE_DIR}/cmake/PostConfig.cmake.in
   ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake @ONLY )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PreConfig.cmake
               ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/PostConfig.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR} )

# Install the export sanitizer script:
install(
   FILES ${CMAKE_SOURCE_DIR}/cmake/skeletons/atlas_export_sanitizer.cmake.in
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules/skeletons )

# Install the Gaudi CPack configuration module:
install( FILES ${CMAKE_SOURCE_DIR}/cmake/GaudiCPackSettings.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Generate the environment setup for the externals. No replacements need to be
# done to it, so it can be used for both the build and the installed release.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Package up the release using CPack:
atlas_cpack_setup()
